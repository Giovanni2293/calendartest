import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { EventAdapter, EventData } from "../domain/event.model";
import { map } from "rxjs/operators";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class EventService {
  private baseUrl = "http://localhost:8080/api/events";

  constructor(private http: HttpClient, private eventAdapter: EventAdapter) {}

  list(): Observable<EventData[]> {
    return this.http
      .get(this.baseUrl)
      .pipe(
        map((data: any[]) => data.map((item) => this.eventAdapter.adapt(item)))
      );
  }

  deleteByEvent(id: Number):Observable<Object>{
    return this.http.delete<Object>(`${this.baseUrl}/${id}`,{observe: 'response'});
  }

  addEvent(event: EventData):Observable<Object>{
    return this.http.post<Object>(`${this.baseUrl}`, event.toJson(), {observe: 'response'});
  }

  editEvent(event: EventData):Observable<Object>{
    return this.http.put<Object>(`${this.baseUrl}`, event.toJson(), {observe: 'response'});
  }
}
