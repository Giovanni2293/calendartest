import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { InstructorAdapter, InstructorData } from '../domain/instructor.model';
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class InstructorService {

  private baseUrl = "http://localhost:8080/api/instructors";

  constructor(private http: HttpClient, private instructorAdapter: InstructorAdapter) { }

  list():Observable<InstructorData[]> {
    return this.http.get(this.baseUrl).pipe(
      map((data: any[]) => data.map((item) => this.instructorAdapter.adapt(item)))
    );
  }
}
