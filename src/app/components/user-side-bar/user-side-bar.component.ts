import { Component, OnInit } from '@angular/core';
import { InstructorData } from 'src/app/domain/instructor.model';
import { InstructorService } from 'src/app/infrastructure/instructor.service';

@Component({
  selector: 'app-user-side-bar',
  templateUrl: './user-side-bar.component.html',
  styleUrls: ['./user-side-bar.component.css']
})
export class UserSideBarComponent implements OnInit {

  instructors:InstructorData[];
  constructor(private instructorService: InstructorService) { }

  ngOnInit(): void {
    this.instructorService.list().subscribe(
      (instructors: InstructorData[])=>{
        this.instructors = instructors;
      }
    );
  }

}
