import {
  Component,
  ChangeDetectionStrategy,
  ViewChild,
  TemplateRef,
  OnInit,
} from "@angular/core";
import { isSameDay, isSameMonth, addHours } from "date-fns";
import { Subject } from "rxjs";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import {
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTimesChangedEvent,
  CalendarView,
} from "angular-calendar";
import { actions } from "../../../constants/constants";
import { EventService } from "src/app/infrastructure/event.service";
import { EventData } from "src/app/domain/event.model";
import { Adapter } from "src/app/domain/adapter";
import { HttpResponse } from "@angular/common/http";
import { InstructorData } from "src/app/domain/instructor.model";
import { InstructorService } from "src/app/infrastructure/instructor.service";
import { MatListOption } from "@angular/material/list";

const colors: any = {
  red: {
    primary: "#ad2121",
    secondary: "#FAE3E3",
  },
  blue: {
    primary: "#1e90ff",
    secondary: "#D1E8FF",
  },
  yellow: {
    primary: "#e3bc08",
    secondary: "#FDF1BA",
  },
};

@Component({
  selector: "app-calendar-main",
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: "./calendar-main.component.html",
  styleUrls: ["./calendar-main.component.css"],
})
export class CalendarMainComponent implements OnInit, Adapter<CalendarEvent> {
  @ViewChild("modalContent", { static: true }) modalContent: TemplateRef<any>;
  @ViewChild("instructorModal", { static: true })
  instructorModal: TemplateRef<any>;

  view: CalendarView = CalendarView.Month;

  CalendarView = CalendarView;

  viewDate: Date = new Date();

  modalData: {
    action: string;
    event: CalendarEvent;
  };

  actions: CalendarEventAction[] = [
    {
      label: '<i class="fas fa-fw fa-pencil-alt"></i>',
      a11yLabel: "Edit",
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.handleEvent(actions.Update, event);
      },
    },
    {
      label: '<i class="fas fa-fw fa-trash-alt"></i>',
      a11yLabel: "Delete",
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.events = this.events.filter((iEvent) => iEvent !== event);
        this.handleEvent(actions.Delete, event);
      },
    },
  ];

  refresh: Subject<any> = new Subject();

  events: CalendarEvent[] = [];
  eventMapping: Map<String, EventData>;

  activeDayIsOpen: boolean;

  instructors: InstructorData[];

  constructor(
    private modal: NgbModal,
    private eventService: EventService,
    private instructorService: InstructorService
  ) {}

  ngOnInit(): void {
    this.loadEvents();
  }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
      this.viewDate = date;
    }
  }

  eventTimesChanged({
    event,
    newStart,
    newEnd,
  }: CalendarEventTimesChangedEvent): void {
    event.start = newStart;
    event.end = newEnd;
    this.events = this.events.map((iEvent) => {
      if (iEvent === event) {
        let editedEvent:CalendarEvent = {
          ...event
        };
        return editedEvent
      }
      return iEvent;
    });
    this.editEvent(event);
  }

  handleEvent(action: string, event: CalendarEvent): void {
    this.modalData = { event, action };
    switch (action) {
      case actions.Create:
        this.addEvent();
        break;
      case actions.Read:
        //not implemented
        break;
      case actions.Update:
        this.modal.open(this.modalContent, { size: "lg" });
        break;
      case actions.Delete:
        this.deleteEvent(event);
        break;
    }
  }

  addEvent(): void {
    let event: EventData = new EventData(
      "New event",
      "New Description",
      colors.red.primary,
      colors.red.secondary,
      new Date(),
      addHours(new Date(), 1)
    );
    this.eventService.addEvent(event).subscribe(
      (response: HttpResponse<Object>) => {
        if (response.status == 201) {
          this.events = [...this.events, this.adapt(event)];
          this.refresh.next();
        }
      },
      (err) => console.log(err)
    );
  }

  saveModal(event: CalendarEvent) {
    this.editEvent(event);
  }

  deleteEvent(eventToDelete: CalendarEvent) {
    this.deleteAction(Number(eventToDelete.id), () => {
      this.events = this.events.filter((event) => event !== eventToDelete);
    });
  }

  editEvent(eventToEdit: CalendarEvent) {
    let event: EventData = new EventData(
      eventToEdit.title,
      "updated description",
      eventToEdit.color.primary,
      eventToEdit.color.secondary,
      eventToEdit.start,
      eventToEdit.end
    );
    this.eventService.editEvent(event).subscribe(
      (response: HttpResponse<Object>) => {
        if (response.status == 200) {
          this.refresh.next();
        }
      },
      (err) => console.log(err)
    );
  }

  deleteAction(id: Number, afterDeleted?: Function) {
    this.eventService.deleteByEvent(id).subscribe(
      (response: HttpResponse<Object>) => {
        if (response.status == 204) {
          if (afterDeleted) afterDeleted();
          this.refresh.next();
        }
      },
      (err) => console.log(err)
    );
  }

  setView(view: CalendarView) {
    this.view = view;
  }

  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
  }

  loadEvents() {
    this.eventService.list().subscribe(
      (events: any[]) => {
        this.events = events.map((item) => this.adapt(item));
        this.refresh.next();
      },
      (err) => console.log(err)
    );
  }

  adapt(item: EventData): CalendarEvent<CalendarEvent> {
    return {
      id: Number(item.getEventId()),
      title: String(item.getEventTitle()),
      start: item.getStartDate(),
      end: item.getEndDate(),
      color: {
        primary: String(item.getEventPrimaryColor()),
        secondary: String(item.getEventSecondaryColor()),
      },
      actions: this.actions,
      resizable: {
        beforeStart: true,
        afterEnd: true,
      },
      draggable: true,
    };
  }

  addAttendant() {
    this.instructorService.list().subscribe((instructors: InstructorData[]) => {
      this.instructors = instructors;
      this.modal.open(this.instructorModal, {
        size: "sm",
        windowClass: "custom-class",
      });
    });
  }

  onGroupsChange(options: MatListOption[], closeCallback: Function) {
    let selectedInstructor: InstructorData = options.map((o) => o.value)[0];

    /*this.instructors = this.instructors.filter(function(value, index, arr){ 
      return value.getFirstName()!=selectedInstructor.getFirstName();
    });*/
    closeCallback();
  }
}
