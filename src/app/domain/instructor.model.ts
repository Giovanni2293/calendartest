import { Injectable } from "@angular/core";
import { Adapter } from "./adapter";

export class InstructorData {
  private documentType: String;
  private documentNumber: Number;
  private birthday: Date;
  private firstName: String;
  private lastName: String;

  public constructor(
    documentType: String,
    documentNumber: Number,
    birthday: Date,
    firstName: String,
    lastName: String
  ) {
    this.setDocumentType(documentType);
    this.setDocumentNumber(documentNumber);
    this.setBirthday(birthday);
    this.setFirstName(firstName);
    this.setLastName(lastName);
  }

  public getDocumentType(): String {
    return this.documentType;
  }
  public setDocumentType(value: String) {
    this.documentType = value;
  }
  public getDocumentNumber(): Number {
    return this.documentNumber;
  }
  public setDocumentNumber(value: Number) {
    this.documentNumber = value;
  }
  public getBirthday(): Date {
    return this.birthday;
  }
  public setBirthday(value: Date) {
    this.birthday = value;
  }
  public getFirstName(): String {
    return this.firstName;
  }
  public setFirstName(value: String) {
    this.firstName = value;
  }
  public getLastName(): String {
    return this.lastName;
  }
  public setLastName(value: String) {
    this.lastName = value;
  }
}

@Injectable({
  providedIn: "root",
})
export class InstructorAdapter implements Adapter<InstructorData> {
  adapt(item: any): InstructorData {
    return new InstructorData(
      item.document,
      item.documentType,
      item.birthday,
      item.firstName,
      item.lastName
    );
  }
}
