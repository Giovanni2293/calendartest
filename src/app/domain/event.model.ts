import { Injectable } from "@angular/core";
import { Adapter } from "./adapter";

export class EventData {
  private eventId: Number;
  private eventTitle: String;
  private eventDescription: String;
  private eventPrimaryColor: String;
  private eventSecondaryColor: String;
  private startDate: Date;
  private endDate: Date;

  public constructor(
    eventTitle: String,
    eventDescription: String,
    eventPrimaryColor: String,
    eventSecondaryColor: String,
    startDate: Date,
    endDate: Date,
    eventId?: Number
  ) {
    this.setEventId(eventId);
    this.setEventTitle(eventTitle);
    this.setEventDescription(eventDescription);
    this.setEventPrimaryColor(eventPrimaryColor);
    this.setEventSecondaryColor(eventSecondaryColor);
    this.setStartDate(startDate);
    this.setEndDate(endDate);
  }

  public getEventId():Number{
    return this.eventId;
  }

  public setEventId(value: Number){
    this.eventId = value;
  }

  public getEventTitle(): String {
    return this.eventTitle;
  }
  public setEventTitle(value: String) {
    this.eventTitle = value;
  }
  public getEventDescription(): String {
    return this.eventDescription;
  }
  public setEventDescription(value: String) {
    this.eventDescription = value;
  }
  public getEventPrimaryColor(): String {
    return this.eventPrimaryColor;
  }
  public setEventPrimaryColor(value: String) {
    this.eventPrimaryColor = value;
  }
  public getEventSecondaryColor(): String {
    return this.eventSecondaryColor;
  }
  public setEventSecondaryColor(value: String) {
    this.eventSecondaryColor = value;
  }
  public getStartDate(): Date {
    return this.startDate;
  }
  public setStartDate(value: Date) {
    this.startDate = new Date(value);
  }
  public getEndDate(): Date {
    return this.endDate;
  }
  public setEndDate(value: Date) {
    this.endDate = new Date(value);
  }

  public toJson(){
    return {
      'eventTitle':this.eventTitle,
      'eventDescription':this.eventDescription,
      'eventPrimaryColor':this.eventPrimaryColor,
      'eventSecondaryColor':this.eventSecondaryColor,
      'startDate':this.startDate,
      'endDate':this.endDate
    };
  }
}

@Injectable({
  providedIn: "root",
})
export class EventAdapter implements Adapter<EventData> {
  adapt(item: any): EventData {
    return new EventData(
      item.eventTitle,
      item.eventDescription,
      item.eventPrimaryColor,
      item.eventSecondaryColor,
      item.startDate,
      item.endDate,
      item.eventId
    );
  }
}
