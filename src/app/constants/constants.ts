export enum actions{
    Create = "create",
    Read = "read",
    Update = "update",
    Delete = "delete",
}