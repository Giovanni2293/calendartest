import { BrowserModule } from '@angular/platform-browser';
import { LOCALE_ID, NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';


import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FlatpickrModule } from 'angularx-flatpickr';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { CalendarMainComponent } from './components/widgets/calendar-main/calendar-main.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UserSideBarComponent } from './components/user-side-bar/user-side-bar.component';
import {MatListModule} from '@angular/material/list';
import {HttpClientModule} from '@angular/common/http'
import { InstructorService } from './infrastructure/instructor.service';
import { EventService } from './infrastructure/event.service';

@NgModule({
  declarations: [
    AppComponent,
    CalendarMainComponent,
    UserSideBarComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CommonModule,
    FormsModule,
    NgbModalModule,
    FlatpickrModule.forRoot(),
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory,
    }),
    BrowserAnimationsModule,
    MatListModule,
    HttpClientModule
  ],
  providers: [InstructorService, EventService],
  bootstrap: [AppComponent]
})
export class AppModule { }
