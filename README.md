## steps to Setup

1. Download and install NodeJS with npm(included by default) https://nodejs.org/es/
2. Once NodeJS is installed, execute `npm install -g @angular/cli`
3. Make a directory and navegate into. After that, execute `git clone https://gitlab.com/Giovanni2293/calendartest.git`
4. After, go into the folder that git clonned called calendartest with a terminal `cd calendartest` and execute `npm install`
5. Finally, you can execute `ng serve -o`. That command will open your defalt browser and open the site. You can also switch browser just using the url provided from `ng serve` that is http://localhost:4200/